// querySelector() method returns the first element that matches a selector
const txtFirstName = document.querySelector('#txt-first-name');

// Activity
const txtLastName = document.querySelector('#txt-last-name');

const spanFullName = document.querySelector('#span-full-name');

// addEventListener() method attaches an event handler to an element
// keyup - a keyboard key is released after being pushed


txtFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = txtFirstName.value;
    // spanFullName.innerHTML = txtLastName.value;
});

// Activity
txtLastName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML =  txtFirstName.value + ' ' + txtLastName.value;
});
/* 
    What is an event target value?
    - event.target - gives you the element that triggered the event
    - event.target.value - return the element where the event occurred
*/
txtFirstName.addEventListener('keyup', (event) => {
    // the target property is read-only
    console.log(event.target);
    console.log(event.target.value);
});






